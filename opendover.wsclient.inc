<?php

/**
 * TODO: write descr.
 * @file
 */


/**
 *
 */
function load_subject_domains_from_server() {
  include_once 'OpenDoverService.php';
  try {
    $subject_domains = array();

    /* Init OpenDover connection settings for wsdl */

    $service_open_dover_login = variable_get('opendover_wsdlservice_login', '');
    $service_open_dover_password = variable_get('opendover_wsdlservice_password', '');
    if (!OPENDOVER_SERVICE_URL_HARDCODED) {
      $service_open_dover_url = variable_get('opendover_wsdlservice_url', OPENDOVER_SERVICE_URL);
    }
    else {
      $service_open_dover_url = OPENDOVER_SERVICE_URL;
    }
    $options = array(
      'login' => $service_open_dover_login,
      'password' => $service_open_dover_password,
      'authentication' => SOAP_AUTHENTICATION_BASIC,
      'soap_version' => SOAP_1_1,
      'cache_wsdl' => WSDL_CACHE_NONE,
      'location' => $service_open_dover_url
    );


    $service_open_dover_api_key = variable_get('opendover_license_id', NULL);
    $get_available_subject_domains = new getAvailableSubjectDomains();
    $get_available_subject_domains->apiKey = $service_open_dover_api_key;

    $service = new OpenDoverService(drupal_get_path('module', 'opendover') . '/OpenDoverService.wsdl', $options);
    $result = $service->getAvailableSubjectDomains($get_available_subject_domains);


    if ($result) {
      if (is_array($result->return)) {
        foreach ($result->return as $subject_domain) {
          $subject_domains[$subject_domain->code] = $subject_domain->name;
        }
      }
      else {
        $subject_domain = $result->return;
        $subject_domains[$subject_domain->code] = $subject_domain->name;
      }
    }
    if (count($subject_domains) == 0) {
      watchdog('opendover', "Opendover fetch subject domains failed. No data recevied", array(), WATCHDOG_ERROR);
    }
    return $subject_domains;

  }
  catch (Exception $e) {
    // in case of an error, process the fault
    if ($e instanceof SoapFault) {
      watchdog('opendover', "Opendover fetch subject domains failed. Soap Fault: !error", array('!error' => $e->getMessage()), WATCHDOG_ERROR);
    }
    else {
      watchdog('opendover', "Opendover fetch subject domains failed. Message = !error", array('!error' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }
}
