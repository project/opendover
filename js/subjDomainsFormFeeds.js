
function opendover_mode_changed() {
  var x = $("#opendover-subj-domains-form-feeds");
  $(":checkbox", x).attr("disabled", $("#edit-opendover-mode-accurate", x).attr("checked") ? null : "disabled");
}

Drupal.behaviors.subj_domains_form_feeds = function(context) {
  $("#opendover-subj-domains-form-feeds :radio").click(opendover_mode_changed);
  opendover_mode_changed();
};
