
Drupal.behaviors.opendover_interactive_markup = function(context) {
  $("strong.opendover-markup").bind('mouseenter',
                                   function(el) {
                                     var rel = $(this).attr('rel');

                                     var rels = rel.split(' ');
                                     for (var key in rels) {
                                       var val = rels [key];
                                       $('strong.opendover-markup#opendover_' + val).addClass('hovered');
                                     }
                                   }).bind('mouseleave', function(el) {
    var rel = $(this).attr('rel');
    var rels = rel.split(' ');
    for (var key in rels) {
      var val = rels [key];
      $('strong.opendover-markup#opendover_' + val).removeClass('hovered');
    }
  });
};
