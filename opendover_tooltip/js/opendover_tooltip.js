
//How far should tool tip box appear from the mouse?
var offsetX = -40;
var offsetY = -5;

//When document is finished process tooltipping functionality
$(document).ready(function() {


  $('<div id="desctip"><div class=text></div><div class=arrow></div></div>')
          .appendTo('body')
          .hide();//.bind('mouseenter',function() {$(this).hide();});
  $('strong.opendover-markup[title]').each(
                                          function() {
                                            var $this = $(this);
                                            $this.data('title', $this.attr('title'));
                                            $this.removeAttr('title');
                                          }).bind('mouseenter',
                                                 function(e) {
                                                   //Create HTML to Tooltipify
                                                   var helptext = $.url.decode($(this).data('title'));
                                                   //Append to page at current mouse position
                                                   form_tooltipsAppend(helptext, $(this));
                                                 }).bind('mouseleave', function(e) {
    $('#desctip').hide();
  });


});


//Append text to body (absolutely positioned)
function form_tooltipsAppend(text, e) {

  $('#desctip .text').html(text);
  $('#desctip').show()
          .css('top', e.offset().top - $('#desctip').outerHeight() + offsetY)
          .css('left', e.offset().left + offsetX + e.width() / 2);
  $('#desctip');

}

/*! Copyright (c) 2008 Brandon Aaron (http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version: 1.0.3
 * Requires jQuery 1.1.3+
 * Docs: http://docs.jquery.com/Plugins/livequery
 */

(function($) {

  $.extend($.fn, {
    livequery: function(type, fn, fn2) {
      var self = this, q;

      // Handle different call patterns
      if ($.isFunction(type))
        fn2 = fn,fn = type,type = undefined;

      // See if Live Query already exists
      $.each($.livequery.queries, function(i, query) {
        if (self.selector == query.selector && self.context == query.context &&
                type == query.type && (!fn || fn.$lqguid == query.fn.$lqguid) && (!fn2 || fn2.$lqguid == query.fn2.$lqguid))
        // Found the query, exit the each loop
          return (q = query) && false;
      });

      // Create new Live Query if it wasn't found
      q = q || new $.livequery(this.selector, this.context, type, fn, fn2);

      // Make sure it is running
      q.stopped = false;

      // Run it immediately for the first time
      q.run();

      // Contnue the chain
      return this;
    },

    expire: function(type, fn, fn2) {
      var self = this;

      // Handle different call patterns
      if ($.isFunction(type))
        fn2 = fn,fn = type,type = undefined;

      // Find the Live Query based on arguments and stop it
      $.each($.livequery.queries, function(i, query) {
        if (self.selector == query.selector && self.context == query.context &&
                (!type || type == query.type) && (!fn || fn.$lqguid == query.fn.$lqguid) && (!fn2 || fn2.$lqguid == query.fn2.$lqguid) && !this.stopped)
          $.livequery.stop(query.id);
      });

      // Continue the chain
      return this;
    }
  });

  $.livequery = function(selector, context, type, fn, fn2) {
    this.selector = selector;
    this.context = context || document;
    this.type = type;
    this.fn = fn;
    this.fn2 = fn2;
    this.elements = [];
    this.stopped = false;

    // The id is the index of the Live Query in $.livequery.queries
    this.id = $.livequery.queries.push(this) - 1;

    // Mark the functions for matching later on
    fn.$lqguid = fn.$lqguid || $.livequery.guid++;
    if (fn2) fn2.$lqguid = fn2.$lqguid || $.livequery.guid++;

    // Return the Live Query
    return this;
  };

  $.livequery.prototype = {
    stop: function() {
      var query = this;

      if (this.type)
      // Unbind all bound events
        this.elements.unbind(this.type, this.fn);
      else if (this.fn2)
      // Call the second function for all matched elements
        this.elements.each(function(i, el) {
          query.fn2.apply(el);
        });

      // Clear out matched elements
      this.elements = [];

      // Stop the Live Query from running until restarted
      this.stopped = true;
    },

    run: function() {
      // Short-circuit if stopped
      if (this.stopped) return;
      var query = this;

      var oEls = this.elements,
              els = $(this.selector, this.context),
              nEls = els.not(oEls);

      // Set elements to the latest set of matched elements
      this.elements = els;

      if (this.type) {
        // Bind events to newly matched elements
        nEls.bind(this.type, this.fn);

        // Unbind events to elements no longer matched
        if (oEls.length > 0)
          $.each(oEls, function(i, el) {
            if ($.inArray(el, els) < 0)
              $.event.remove(el, query.type, query.fn);
          });
      }
      else {
        // Call the first function for newly matched elements
        nEls.each(function() {
          query.fn.apply(this);
        });

        // Call the second function for elements no longer matched
        if (this.fn2 && oEls.length > 0)
          $.each(oEls, function(i, el) {
            if ($.inArray(el, els) < 0)
              query.fn2.apply(el);
          });
      }
    }
  };

  $.extend($.livequery, {
    guid: 0,
    queries: [],
    queue: [],
    running: false,
    timeout: null,

    checkQueue: function() {
      if ($.livequery.running && $.livequery.queue.length) {
        var length = $.livequery.queue.length;
        // Run each Live Query currently in the queue
        while (length--)
          $.livequery.queries[ $.livequery.queue.shift() ].run();
      }
    },

    pause: function() {
      // Don't run anymore Live Queries until restarted
      $.livequery.running = false;
    },

    play: function() {
      // Restart Live Queries
      $.livequery.running = true;
      // Request a run of the Live Queries
      $.livequery.run();
    },

    registerPlugin: function() {
      $.each(arguments, function(i, n) {
        // Short-circuit if the method doesn't exist
        if (!$.fn[n]) return;

        // Save a reference to the original method
        var old = $.fn[n];

        // Create a new method
        $.fn[n] = function() {
          // Call the original method
          var r = old.apply(this, arguments);

          // Request a run of the Live Queries
          $.livequery.run();

          // Return the original methods result
          return r;
        }
      });
    },

    run: function(id) {
      if (id != undefined) {
        // Put the particular Live Query in the queue if it doesn't already exist
        if ($.inArray(id, $.livequery.queue) < 0)
          $.livequery.queue.push(id);
      }
      else
      // Put each Live Query in the queue if it doesn't already exist
        $.each($.livequery.queries, function(id) {
          if ($.inArray(id, $.livequery.queue) < 0)
            $.livequery.queue.push(id);
        });

      // Clear timeout if it already exists
      if ($.livequery.timeout) clearTimeout($.livequery.timeout);
      // Create a timeout to check the queue and actually run the Live Queries
      $.livequery.timeout = setTimeout($.livequery.checkQueue, 20);
    },

    stop: function(id) {
      if (id != undefined)
      // Stop are particular Live Query
        $.livequery.queries[ id ].stop();
      else
      // Stop all Live Queries
        $.each($.livequery.queries, function(id) {
          $.livequery.queries[ id ].stop();
        });
    }
  });

// Register core DOM manipulation methods
  $.livequery.registerPlugin('append', 'prepend', 'after', 'before', 'wrap', 'attr', 'removeAttr', 'addClass', 'removeClass', 'toggleClass', 'empty', 'remove');

// Run Live Queries when the Document is ready
  $(function() {
    $.livequery.play();
  });


// Save a reference to the original init method
  var init = $.prototype.init;

// Create a new init method that exposes two new properties: selector and context
  $.prototype.init = function(a, c) {
    // Call the original init and save the result
    var r = init.apply(this, arguments);

    // Copy over properties if they exist already
    if (a && a.selector)
      r.context = a.context,r.selector = a.selector;

    // Set properties
    if (typeof a == 'string')
      r.context = c || document,r.selector = a;

    // Return the result
    return r;
  };

// Give the init function the jQuery prototype for later instantiation (needed after Rev 4091)
  $.prototype.init.prototype = $.prototype;

})(jQuery);

jQuery.url = function() {

  /**
   * private function to encode URL
   *
   * @param {String} string //required
   * @return {String}
   */
  function utf8_encode(string) {
    string = string.replace(/\r\n/g, "\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {

      var c = string.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      }
      else if ((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      }
      else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }

    }

    return utftext;
  }

  /**
   * private function to decode URL
   *
   * @param {String} utftext //required
   * @return {String}
   */
  function utf8_decode(utftext) {
    var string = "";
    var i = 0;
    var c = 0;
    var c2 = 0;

    while (i < utftext.length) {

      c = utftext.charCodeAt(i);

      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      }
      else if ((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i + 1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }
      else {
        c2 = utftext.charCodeAt(i + 1);
        c3 = utftext.charCodeAt(i + 2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }

    }

    return string;
  }


  /**
   * private function to convert urlencoded query string to javascript object
   *
   * @param {String} params //required
   * @param {Boolean} coerce //optional
   * @return {Object}
   *
   * @author Ben Alman
   */
  function deparam(params, coerce) {
    var obj = {},
            coerce_types = {
              'true': !0,
              'false': !1,
              'null': null
            };

    // Iterate over all name=value pairs.
    $.each(params.replace(/\+/g, ' ').split('&'), function (j, v) {
      var param = v.split('='),
              key = decode(param[0]),
              val, cur = obj,
              i = 0,

        // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
        // into its component parts.
              keys = key.split(']['),
              keys_last = keys.length - 1;

      // If the first keys part contains [ and the last ends with ], then []
      // are correctly balanced.
      if (/\[/.test(keys[0]) && /\]$/.test(keys[keys_last])) {
        // Remove the trailing ] from the last keys part.
        keys[keys_last] = keys[keys_last].replace(/\]$/, '');

        // Split first keys part into two parts on the [ and add them back onto
        // the beginning of the keys array.
        keys = keys.shift().split('[').concat(keys);

        keys_last = keys.length - 1;
      } else {
        // Basic 'foo' style key.
        keys_last = 0;
      }

      // Are we dealing with a name=value pair, or just a name?
      if (param.length === 2) {
        val = decode(param[1]);

        // Coerce values.
        if (coerce) {
          val = val && !isNaN(val) ? +val // number
                  : val === 'undefined' ? undefined // undefined
                  : coerce_types[val] !== undefined ? coerce_types[val] // true, false, null
                  : val; // string
        }

        if (keys_last) {
          // Complex key, build deep object structure based on a few rules:
          // * The 'cur' pointer starts at the object top-level.
          // * [] = array push (n is set to array length), [n] = array if n is
          //   numeric, otherwise object.
          // * If at the last keys part, set the value.
          // * For each keys part, if the current level is undefined create an
          //   object or array based on the type of the next keys part.
          // * Move the 'cur' pointer to the next level.
          // * Rinse & repeat.
          for (; i <= keys_last; i++) {
            key = keys[i] === '' ? cur.length : keys[i];
            cur = cur[key] = i < keys_last ? cur[key] || (keys[i + 1] && isNaN(keys[i + 1]) ? {} : []) : val;
          }

        } else {
          // Simple key, even simpler rules, since only scalars and shallow
          // arrays are allowed.
          if ($.isArray(obj[key])) {
            // val is already an array, so push on the next value.
            obj[key].push(val);

          } else if (obj[key] !== undefined) {
            // val isn't an array, but since a second value has been specified,
            // convert val into an array.
            obj[key] = [obj[key], val];

          } else {
            // val is a scalar.
            obj[key] = val;
          }
        }

      } else if (key) {
        // No value was defined, so set something meaningful.
        obj[key] = coerce ? undefined : '';
      }
    });

    return obj;
  }

  /**
   * private function to parse URL to components
   *
   * @param {String} url_str //optional, if omited using current location
   * @return {Object}
   */
  function parse(url_str) {
    url_str = url_str || window.location;

    /**
     * @author of RegExp Steven Levithan
     */
    var re = /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/;

    var keys = ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"];

    var m = re.exec(url_str);
    var uri = {};
    var i = keys.length;

    while (i--) {
      uri[ keys[i] ] = m[i] || "";
    }
    /*
     uri.params = {};

     uri.query.replace( /(?:^|&)([^&=]*)=?([^&]*)/g, function ( $0, $1, $2 ) {
     if ($1) {
     uri.params[decode($1)] = decode($2);
     }
     });
     */
    if (uri.query) {
      uri.params = deparam(uri.query, true);
    }

    return uri;
  }


  /**
   * private function to build URL string from components
   *
   * @param {Object} url_obj //required
   * @return {String}
   */
  function build(url_obj) {

    if (url_obj.source) {
      return encodeURI(url_obj.source);
    }

    var resultArr = [];

    if (url_obj.protocol) {
      if (url_obj.protocol == 'file') {
        resultArr.push('file:///');
      } else if (url_obj.protocol == 'mailto') {
        resultArr.push('mailto:');
      } else {
        resultArr.push(url_obj.protocol + '://');
      }
    }

    if (url_obj.authority) {
      resultArr.push(url_obj.authority);
    } else {
      if (url_obj.userInfo) {
        resultArr.push(url_obj.userInfo + '@');
      } else if (url_obj.user) {
        resultArr.push(url_obj.user);
        if (url_obj.password) {
          resultArr.push(':' + url_obj.password);
        }
        resultArr.push('@');
      }

      if (url_obj.host) {
        resultArr.push(url_obj.host);
        if (url_obj.port) {
          resultArr.push(':' + url_obj.port);
        }
      }
    }

    if (url_obj.path) {
      resultArr.push(url_obj.path);
    } else {
      if (url_obj.directory) {
        resultArr.push(url_obj.directory);
      }
      if (url_obj.file) {
        resultArr.push(url_obj.file);
      }

    }

    if (url_obj.query) {
      resultArr.push('?' + url_obj.query);
    } else if (url_obj.params) {
      resultArr.push('?' + $.param(url_obj.params));
    }

    if (url_obj.anchor) {
      resultArr.push('#' + url_obj.anchor);
    }

    return resultArr.join('');
  }

  /**
   * wrapper around encoder
   *
   * @param {String} string //required
   * @return {String}
   */
  function encode(string) {
    //return build(parse(string));
    //return escape(utf8_encode(string));
    return encodeURIComponent(string);
  }

  /**
   * wrapper around decoder
   *
   * @param {String} string //optional, if omited using current location
   * @return {String}
   */
  function decode(string) {
    string = string || window.location.toString();
    return utf8_decode(unescape(string.replace(/\+/g, ' ')));
  }

  /**
   * public functions
   *
   * @see #encode
   * @see #decode
   * @see #parse
   * @see #build
   *
   * @return {Object}
   */
  return {
    encode: encode,
    decode: decode,
    parse: parse,
    build: build
  };
}();
