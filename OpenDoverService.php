<?php
class searchBareSentiments {
  public $apiKey; // string
  public $text; // string
}

class searchBareSentimentsResponse {
  public $return; // srchBareSentimentsResponse
}

class srchBareSentimentsResponse {
  public $bareSentiments; // odBareSentiment
}

class odBareSentiment {
  public $sentimentForce; // int
  public $sentimentType; // odSentimentType
  public $sentimentWord; // odFrame
}

class odFrame {
  public $cleanText; // string
  public $length; // int
  public $offset; // int
}

class OpenDoverFault {
  public $code; // int
  public $message; // string
}

class getAvailableSubjectDomains {
  public $apiKey; // string
}

class getAvailableSubjectDomainsResponse {
  public $return; // odSubjectDomain
}

class odSubjectDomain {
  public $code; // string
  public $name; // string
}

class searchObjectSentiments {
  public $apiKey; // string
  public $text; // string
  public $sentimentObjects; // string
}

class searchObjectSentimentsResponse {
  public $return; // srchObjectSentimentsResponse
}

class srchObjectSentimentsResponse {
  public $domainRanks; // odDomainRank
  public $domainWords; // odDomainWord
  public $objectSentiments; // odObjectSentiment
}

class odDomainRank {
  public $domainCode; // string
  public $rank; // int
}

class odDomainWord {
  public $domainCodes; // string
  public $domainWord; // odFrame
}

class odObjectSentiment {
  public $domainCodes; // string
  public $domainWords; // odFrame
  public $sentimentForce; // int
  public $sentimentObject; // odFrame
  public $sentimentObjectIndex; // int
  public $sentimentType; // odSentimentType
  public $sentimentWord; // odFrame
}

class getSubjectDomainByCode {
  public $apiKey; // string
  public $code; // string
}

class getSubjectDomainByCodeResponse {
  public $return; // odSubjectDomain
}

class searchBareObjectSentiments {
  public $apiKey; // string
  public $text; // string
  public $sentimentObjects; // string
}

class searchBareObjectSentimentsResponse {
  public $return; // srchBareObjectSentimentsResponse
}

class srchBareObjectSentimentsResponse {
  public $bareObjectSentiments; // odBareObjectSentiment
}

class odBareObjectSentiment {
  public $sentimentForce; // int
  public $sentimentObject; // odFrame
  public $sentimentObjectIndex; // int
  public $sentimentType; // odSentimentType
  public $sentimentWord; // odFrame
}

class detectSubjectDomain {
  public $apiKey; // string
  public $text; // string
}

class detectSubjectDomainResponse {
  public $return; // odDetectSubjectDomainResponse
}

class odDetectSubjectDomainResponse {
  public $domainRanks; // odDomainRank
  public $domainWords; // odDomainWord
}

class searchSentiments {
  public $apiKey; // string
  public $text; // string
}

class searchSentimentsResponse {
  public $return; // srchSentimentsResponse
}

class srchSentimentsResponse {
  public $domainRanks; // odDomainRank
  public $domainWords; // odDomainWord
  public $sentiments; // odSentiment
}

class odSentiment {
  public $domainCodes; // string
  public $domainWords; // odFrame
  public $sentimentForce; // int
  public $sentimentType; // odSentimentType
  public $sentimentWord; // odFrame
}

class ping {
  public $apiKey; // string
}

class pingResponse {
  public $return; // string
}

class odSentimentType {
  const JUDGEMENT = 'JUDGEMENT';
  const APPRECIATION = 'APPRECIATION';
  const EMOTIONAL_STATE = 'EMOTIONAL_STATE';
}


/**
 * OpenDoverService class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class OpenDoverService extends SoapClient {

  private static $classmap = array(
                                    'searchBareSentiments' => 'searchBareSentiments',
                                    'searchBareSentimentsResponse' => 'searchBareSentimentsResponse',
                                    'srchBareSentimentsResponse' => 'srchBareSentimentsResponse',
                                    'odBareSentiment' => 'odBareSentiment',
                                    'odFrame' => 'odFrame',
                                    'OpenDoverFault' => 'OpenDoverFault',
                                    'getAvailableSubjectDomains' => 'getAvailableSubjectDomains',
                                    'getAvailableSubjectDomainsResponse' => 'getAvailableSubjectDomainsResponse',
                                    'odSubjectDomain' => 'odSubjectDomain',
                                    'searchObjectSentiments' => 'searchObjectSentiments',
                                    'searchObjectSentimentsResponse' => 'searchObjectSentimentsResponse',
                                    'srchObjectSentimentsResponse' => 'srchObjectSentimentsResponse',
                                    'odDomainRank' => 'odDomainRank',
                                    'odDomainWord' => 'odDomainWord',
                                    'odObjectSentiment' => 'odObjectSentiment',
                                    'getSubjectDomainByCode' => 'getSubjectDomainByCode',
                                    'getSubjectDomainByCodeResponse' => 'getSubjectDomainByCodeResponse',
                                    'searchBareObjectSentiments' => 'searchBareObjectSentiments',
                                    'searchBareObjectSentimentsResponse' => 'searchBareObjectSentimentsResponse',
                                    'srchBareObjectSentimentsResponse' => 'srchBareObjectSentimentsResponse',
                                    'odBareObjectSentiment' => 'odBareObjectSentiment',
                                    'detectSubjectDomain' => 'detectSubjectDomain',
                                    'detectSubjectDomainResponse' => 'detectSubjectDomainResponse',
                                    'odDetectSubjectDomainResponse' => 'odDetectSubjectDomainResponse',
                                    'searchSentiments' => 'searchSentiments',
                                    'searchSentimentsResponse' => 'searchSentimentsResponse',
                                    'srchSentimentsResponse' => 'srchSentimentsResponse',
                                    'odSentiment' => 'odSentiment',
                                    'ping' => 'ping',
                                    'pingResponse' => 'pingResponse',
                                    'odSentimentType' => 'odSentimentType',
                                   );

  public function OpenDoverService($wsdl = "opendover_api_v1_soap.xml", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param ping $parameters
   * @return pingResponse
   */
  public function ping(ping $parameters) {
    return $this->__soapCall('ping', array($parameters),       array(
            'uri' => 'stubs.api.opendover.byelex.nl',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getSubjectDomainByCode $parameters
   * @return getSubjectDomainByCodeResponse
   */
  public function getSubjectDomainByCode(getSubjectDomainByCode $parameters) {
    return $this->__soapCall('getSubjectDomainByCode', array($parameters),       array(
            'uri' => 'stubs.api.opendover.byelex.nl',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param searchSentiments $parameters
   * @return searchSentimentsResponse
   */
  public function searchSentiments(searchSentiments $parameters) {
    return $this->__soapCall('searchSentiments', array($parameters),       array(
            'uri' => 'stubs.api.opendover.byelex.nl',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param detectSubjectDomain $parameters
   * @return detectSubjectDomainResponse
   */
  public function detectSubjectDomain(detectSubjectDomain $parameters) {
    return $this->__soapCall('detectSubjectDomain', array($parameters),       array(
            'uri' => 'stubs.api.opendover.byelex.nl',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param searchObjectSentiments $parameters
   * @return searchObjectSentimentsResponse
   */
  public function searchObjectSentiments(searchObjectSentiments $parameters) {
    return $this->__soapCall('searchObjectSentiments', array($parameters),       array(
            'uri' => 'stubs.api.opendover.byelex.nl',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param searchBareSentiments $parameters
   * @return searchBareSentimentsResponse
   */
  public function searchBareSentiments(searchBareSentiments $parameters) {
    return $this->__soapCall('searchBareSentiments', array($parameters),       array(
            'uri' => 'stubs.api.opendover.byelex.nl',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param searchBareObjectSentiments $parameters
   * @return searchBareObjectSentimentsResponse
   */
  public function searchBareObjectSentiments(searchBareObjectSentiments $parameters) {
    return $this->__soapCall('searchBareObjectSentiments', array($parameters),       array(
            'uri' => 'stubs.api.opendover.byelex.nl',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getAvailableSubjectDomains $parameters
   * @return getAvailableSubjectDomainsResponse
   */
  public function getAvailableSubjectDomains(getAvailableSubjectDomains $parameters) {
    return $this->__soapCall('getAvailableSubjectDomains', array($parameters),       array(
            'uri' => 'stubs.api.opendover.byelex.nl',
            'soapaction' => ''
           )
      );
  }

}

?>
