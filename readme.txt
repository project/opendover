Opendover is a Drupal module that uses Mashery API (http://opendover.nl/) and that allows you to extract the next generation semantic features within your blogs, content management systems, websites or other numerous applications.

Installation:

1. Copy module to Drupal installation and enable it
2. Create Vocabularies for storing Subject Domains & Domain Words. Additionally you can add vocabulary for positive Sentiments and negative Sentiments (can be one vocabulary). Make sure you have at least one vocabulary to be used as 'Base Tags' vocabulary too.
3. Go to /admin/settings/opendover to configure Opendover module. 

a). Configure WSDL. 
You should get a Mashery API KEY(http://developer.opendover.nl/apps/register) and fill in webservice URL(`Endpoint URL` from http://developer.opendover.nl/docs). Currently it is `http://api.opendover.nl/api_v1/soap`

b). Configure limitations
Different types of Mashery accounts has limitations on API calls(http://opendover.nl/plans-and-pricing) You should fill `per hour` & `per second` limits according to account you have. Setting wrong values won't corrupt module workflow, though will produce lots of `access denied` warnings in the log file if module will try to exceed service limitations.

c). Set up `Subject Domains` vocabulary. This vocabulary will be used to store terms of detected domains. List of supported domains could be found on http://developer.opendover.nl/home

d). Set up service for content type(s)

NOTE: You can hardcode Mashery URL and prevent it from editing, by changing following strings in module file:
17: define('OPENDOVER_SERVICE_URL', 'http://java.opinionmining.nl/api/OpenDoverService');
26: define('OPENDOVER_SERVICE_URL_HARDCODED', TRUE); // Set FALSE to enable debug option of changing API url by user

4. Place `Opendover processing mode` block somewhere to access processing mode configuration.

Modes:

Accurate/Inaccurate

Accurate mode tries to detect Subject Domain of the article. It detects `domain words` and `sentiments` that are linked with them. If sentiment is not linked to domain word it is ignored.
Inaccurate mode doesn't perform Subject Domains & Domain Words detection

Base tags/Ignore tags

Base tags mode limits sentiment detection. Base tags are node terms of configured vocabulary. If detected sentiment is not linked with any of Base Tags of node it is ignored.
Ignore tags mode ignores tags.

Note that `Base tags` are terms that were inputed by user and Domain Words are autodetected.

Additional modules:

opendover_tooltip provides an example of hook_preprocess_opendover_title that is used for advanced theming of tooltips
opendover_views provides numerous filters for views module






