<?php

/**
 * @file
 * Interface between opendover_views.module and views.module.
 */

/**
 * Constant definitions for the various actions.
 */
define('OPENDOVER_VIEWS_ANY_SENTIMENTS', 1);
define('OPENDOVER_VIEWS_DEFINED_SENTIMENTS', 2);
define('OPENDOVER_VIEWS_UNDEFINED_SENTIMENTS', 3);
define('OPENDOVER_VIEWS_POSITIVE_SENTIMENTS', 4);
define('OPENDOVER_VIEWS_NEGATIVE_SENTIMENTS', 5);

/**
 * Opendover filter sentiments handler classes
 */
class opendover_views_handler_filter_sentiment extends views_handler_filter_in_operator {
  /**
   * Sets list of filter variants that can be selected by a user
   *
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Sentiments');
      $options[OPENDOVER_VIEWS_ANY_SENTIMENTS] = t('Any');
      $options[OPENDOVER_VIEWS_DEFINED_SENTIMENTS] = t('Defined');
      $options[OPENDOVER_VIEWS_UNDEFINED_SENTIMENTS] = t('Undefined');
      $options[OPENDOVER_VIEWS_POSITIVE_SENTIMENTS] = t('Positive');
      $options[OPENDOVER_VIEWS_NEGATIVE_SENTIMENTS] = t('Negative');
      $this->value_options = $options;
    }
  }


  /**
   * Modifies filter query by adding custom WHERE conditions
   *
   */
  function query() {
    $this->ensure_my_table();
    if (isset($this->value) && is_array($this->value)) {
      $values = array_values($this->value);
      $fld = "$this->table_alias.$this->real_field";
      switch ($values[0]) {
        case OPENDOVER_VIEWS_UNDEFINED_SENTIMENTS:
          $qry = "ISNULL($fld)";
          break;
        case OPENDOVER_VIEWS_DEFINED_SENTIMENTS:
          $qry = "NOT ISNULL($fld)";
          break;
        case OPENDOVER_VIEWS_POSITIVE_SENTIMENTS:
          $qry = "$fld > 0.0";
          break;
        case OPENDOVER_VIEWS_NEGATIVE_SENTIMENTS:
          $qry = "$fld < 0.0";
          break;
      }
      if ($qry) {
        $this->query->add_where($this->options['group'], $qry);
      }
    }
  }


}
