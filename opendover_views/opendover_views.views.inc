<?php

/**
 * @file
 * Opendover views include file
 */

/**
 * Implementation of hook_views_data().
 */
function opendover_views_views_data() {
  $data = array();
  $data['opendover_node']['table']['group'] = t('Opendover sentiments');
  $data['opendover_node_sentiments']['table']['group'] = t('Opendover sentiments');
  $data['opendover_node_basetags']['table']['group'] = t('Opendover sentiments');
  $data['opendover_node_domainwords']['table']['group'] = t('Opendover sentiments');
  $data['opendover_node_domains']['table']['group'] = t('Opendover sentiments');
  $data['opendover_process_queue']['table']['group'] = t('Opendover status');
  // Explain how this table joins to others.
  $data['opendover_node']['table']['join'] = array(
    // Directly links to node table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  // Explain how this table joins to others.
  $data['opendover_node_sentiments']['table']['join'] = array(
    // Directly links to node table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  // Explain how this table joins to others.
  $data['opendover_node_basetags']['table']['join'] = array(
    // Directly links to node table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['opendover_node_domainwords']['table']['join'] = array(
    // Directly links to node table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['opendover_node_domains']['table']['join'] = array(
    // Directly links to node table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['opendover_process_queue']['table']['join'] = array(
    // Directly links to node table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  // Define the fields.
  $data['opendover_node_sentiments']['force'] = array(
    'title' => t('Sentiment'),
    'help' => t('Sentiment containing force of the words found in the node text'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'opendover_views_handler_filter_sentiment',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['opendover_node']['averageForce'] = array(
    'title' => t('Node sentiment'),
    'help' => t('Sentiment containing average force of sentiments found in the node text'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'opendover_views_handler_filter_nodesentiment',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['opendover_node_basetags']['averageForce'] = array(
    'title' => t('Base tag sentiment'),
    'help' => t('Sentiment containing average force of the base tags found in the node text'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'opendover_views_handler_filter_basetag',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['opendover_node_domainwords']['averageForce'] = array(
    'title' => t('Domain word sentiment'),
    'help' => t('Sentiment containing average force of the domain words found in the node text'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'opendover_views_handler_filter_domainword',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['opendover_node_domains']['averageForce'] = array(
    'title' => t('Subject domain sentiment'),
    'help' => t('Sentiment containing average force of the subject domains found in the node text'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'opendover_views_handler_filter_domain',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['opendover_process_queue']['oid'] = array(
    'title' => t('Processing state'),
    'help' => t('Check if node is processed by opendover API'),
    'field' => array(
      'click sortable' => FALSE,
      'handler' => 'opendover_views_handler_field_process'
    ),
    'filter' => array(
      'handler' => 'opendover_views_handler_filter_process',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function opendover_views_views_handlers() {
  return array(
    'handlers' => array(
      'opendover_views_handler_filter_sentiment' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'opendover_views_handler_filter_nodesentiment' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'opendover_views_handler_filter_basetag' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'opendover_views_handler_filter_domainword' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'opendover_views_handler_filter_domain' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'opendover_views_handler_filter_process' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'opendover_views_handler_field_process' => array(
        'parent' => 'views_handler_field_numeric',
      ),
    ),
  );
}