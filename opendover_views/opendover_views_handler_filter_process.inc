<?php

/**
 * @file
 * Interface between opendover_views.module and views.module.
 */

/**
 * Constant definitions for the various actions.
 */
define('OPENDOVER_VIEWS_NODE_PROCESSED', 1);
define('OPENDOVER_VIEWS_NODE_QUEUED', 2);
define('OPENDOVER_VIEWS_NODE_ALL', 3);


class opendover_views_handler_filter_process extends views_handler_filter_in_operator {
  /**
   * Sets list of filter variants that can be selected by a user
   *
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Processing state');
      $options[OPENDOVER_VIEWS_NODE_PROCESSED] = t('Processed');
      $options[OPENDOVER_VIEWS_NODE_QUEUED] = t('Queued');
      $options[OPENDOVER_VIEWS_NODE_ALL] = t('All');
      $this->value_options = $options;
    }
  }


  /**
   * Modifies filter query by adding custom WHERE conditions
   *
   */
  function query() {
    $this->ensure_my_table();
    if (isset($this->value) && is_array($this->value)) {
      $values = array_values($this->value);
      $fld = "$this->table_alias.$this->real_field";
      switch ($values[0]) {
        case OPENDOVER_VIEWS_NODE_ALL:
          $qry = "TRUE";
          break;
        case OPENDOVER_VIEWS_NODE_PROCESSED:
          $qry = "ISNULL($fld)";
          break;
        case OPENDOVER_VIEWS_NODE_QUEUED:
          $qry = "NOT ISNULL($fld)";
          break;
      }
      if ($qry) {
        $this->query->add_where($this->options['group'], $qry);
      }
    }
  }


}