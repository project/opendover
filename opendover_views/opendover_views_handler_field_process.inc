<?php

/**
 * @file
 * Interface between opendover_views.module and views.module.
 */

/**
 * @defgroup views_field_handlers Views' field handlers
 * @{
 * Handlers to tell Views how to build and display fields.
 *
 */

class opendover_views_handler_field_process extends views_handler_field_numeric {

  function query() {

    $table = $this->query->ensure_table('opendover_process_queue');

    $sql = "SELECT COUNT(oid) FROM {opendover_process_queue} p WHERE p.nid = node.nid";

    $this->query->add_field('', "($sql)", 'count');
    $this->field_alias = 'process';
  }

  function render($values) {
    $txt = $values->count;
    if ($txt == 0) {
      return 1;
    }
    else
      if ($txt) {
        return NULL;
      }
      else {
        return parent::render($values);
      }
  }
}